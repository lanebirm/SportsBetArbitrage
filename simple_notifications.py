#!/usr/bin/env python
# python version 3.8.0

# -*- encoding: utf-8 -*-

import os.path
import requests
import json
import smtplib
import mimetypes
import base64

# Gmail API imports
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build as googleapiclient_discovery_build
from googleapiclient.errors import HttpError

# other imports
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

# load .env file
from dotenv import load_dotenv;
load_dotenv()

# Python 3 import
import simple_notifications_config

# If modifying these scopes, delete the file token.json.
SCOPES = ['https://mail.google.com/']


def pushover(subject, body, image="no"):
    '''
    This functions sends a notification using Pushover
        Args:
            subject (str) : Title of notification.
            body    (str) : Message of notification.
            image   (str) : Image filename of notification.
    '''
    params = {
        'token': simple_notifications_config.PUSHOVER_APP_TOKEN,
        'user': simple_notifications_config.USER_KEY,
        'title': subject,
        'message': body
    }
    if image != "no":
        image = {"attachment": ("image.jpg", open(image, "rb"), "image/jpeg")}
        response = requests.post(
            'https://api.pushover.net/1/messages.json', data=params, files=image)
    else:
        response = requests.post(
            'https://api.pushover.net/1/messages.json', data=params)
    #print (json.dumps(params))
    if response.status_code != 200:
        print('Something went wrong...')
        print(response.content)
    else:
        print('Sending complete')

def email(subject, recipients, body="", attachments=None, input_msg=None, use_gmail_api=True):
    '''
    This functions sends a notification using Email
            Args:
            subject     (str) : Email Subject.
            body        (str or MIMEText) : Email Body.
            recipients  (str) : Email recipients.
            attachments (str) : Email attachments.
            msg         (MIMEMultipart):" message pre created (optional). Ignores body if given
    '''

    print("Sending email to ", recipients)
    if use_gmail_api:
        msg = MIMEMultipart('alternative')
    else:
        msg = MIMEMultipart()

    if input_msg != None:
        msg=input_msg
    else:
        msg.attach(MIMEText(body))
    
    msg['From'] = simple_notifications_config.EMAIL_SENDER
    email_recipients = [recipients]
    msg['To'] = ', '.join(email_recipients)
    msg['Subject'] = subject

    if use_gmail_api:
        email_gmail_api(msg)
    else:
        server = smtplib.SMTP(simple_notifications_config.EMAIL_SERVER + ':' +
                            simple_notifications_config.EMAIL_SERVER_PORT)
        if simple_notifications_config.EMAIL_DEBUG_LEVEL == '1':
            server.set_debuglevel(1)
        server.starttls()
        server.login(simple_notifications_config.EMAIL_SENDER,
                    simple_notifications_config.EMAIL_PASSWORD)
        server.sendmail(simple_notifications_config.EMAIL_SENDER,
                        email_recipients, msg.as_string())
        server.quit()
        print("email sent")

def email_gmail_api(msg):
    '''Function to email via Gmail API'''

    creds = None

    # Create token.json file from ENV variables
    token_json = os.environ["TOKEN"]
    token_dict = json.loads(token_json)

    creds = Credentials.from_authorized_user_info(token_dict, SCOPES)
    
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            config = json.loads(os.environ['CRED'])
            flow = InstalledAppFlow.from_client_config(
                config, SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.json', 'w') as token:
            token.write(creds.to_json())
    try:
        # Send email
        service = googleapiclient_discovery_build('gmail', 'v1', credentials=creds)
        message_raw = {'raw': base64.urlsafe_b64encode(msg.as_string().encode()).decode()} 
        result = SendMessageInternal(service, "me", message_raw)
        return result

    except HttpError as error:
        # TODO(developer) - Handle errors from gmail API.
        print(f'An error occurred: {error}')

def SendMessageInternal(service, user_id, message):
    '''SendMessageInternal'''
    try:
        message = (service.users().messages().send(userId=user_id, body=message).execute())
        print('Message Id: %s' % message['id'])
        return message
    except HttpError as error:
        print('An error occurred: %s' % error)
        return "Error"

if __name__ == '__main__':
    '''Main section for running the file direcly and testing'''
    msg = MIMEMultipart('alternative')
    email_gmail_api(msg)
    pass
